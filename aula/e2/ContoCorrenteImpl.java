package aula.e2;

public class ContoCorrenteImpl implements ContoCorrente {
	private double saldo = 0;
	
	public ContoCorrenteImpl() {
	}

	@Override
	public double saldoAttuale() {
		return this.saldo;
	}

	@Override
	public boolean prelievo(double d) {
		// costo fisso prelievo: = 1.0 se <= 10, 10% se > 10
		return false ;
	}

	@Override
	public void versamento(double d) {
		saldo += d;
	}

	@Override
	public void cambioAnno() {
		// TODO Auto-generated method stub
		
	}
}
