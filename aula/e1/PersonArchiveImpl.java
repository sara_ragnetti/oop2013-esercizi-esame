package aula.e1;

import java.util.HashMap;
import java.util.Map;

public class PersonArchiveImpl implements PersonArchive {
	private final Map<Integer, String> archive;
	private int limit;
	
	public PersonArchiveImpl() {
		this.archive = new HashMap<>();
	}
	/* gli int di default sono settati a 0. 
	 * Quindi,alla prima add controllo this.limit >= size() con entrambi i valori = 0 
	 * viene lanciata eccezione, ma non dovrebbe succedere.			
	 */
	@Override
	public void addPerson(int code, String name) {
		if (name == null || this.archive.containsKey(code)) {
			throw new IllegalArgumentException();
		} else if(this.limit != 0 && this.limit >= this.archive.size()){  
			throw new UnsupportedOperationException();
		} 
		else {
			this.archive.put(code, name);
		}
	}

	@Override
	public boolean hasCode(int code) {
		return this.archive.containsKey(code);
	}

	@Override
	public String getNameByCode(int code) {
		return this.archive.containsKey(code) ? this.archive.get(code) : null;
	}

	@Override
	public String[] getNames() {
		String names = "";
		for(String s: archive.values()) {
			names = names + s + " ";
		}
		return names.split(" ");
	}

	@Override
	public void setLimit(int n) {
		//posso anche non mettere n<0
		if(n < 0 || n < this.archive.size()) {
			throw new IllegalArgumentException();
		} else {
			this.limit = n;
		}
	}

}
