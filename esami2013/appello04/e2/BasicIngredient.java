package esami2013.appello04.e2;

public class BasicIngredient extends IngredientDecorator {
	private final String name;
	private final int cost;
	
	public BasicIngredient(String name, int cost,Pizza p) {
		super(p);
		this.name = name;
		this.cost = cost;		
	}
	
	//costo della pizza decorator + costo ingrediente aggiunto
	public int getCost() {
		return super.getCost()+this.cost;  
	}
	
	// nome pizza decorator + nome ingrediente aggiunto
	public String getIngredient() {
		return super.getIngredients()+ "," +this.name;
	}
	
}
