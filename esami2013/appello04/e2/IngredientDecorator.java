package esami2013.appello04.e2;

// classe che decora una Pizza 
public class IngredientDecorator implements Pizza {
	protected final Pizza decorated;
	
	protected IngredientDecorator(Pizza decorated) {
		this.decorated = decorated;
	}
	
	@Override
	public int getCost() {
		return this.decorated.getCost();
	}

	@Override
	public String getIngredients() {
		return this.decorated.getIngredients();
	}

}
