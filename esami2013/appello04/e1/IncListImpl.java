package esami2013.appello04.e1;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class IncListImpl<X> implements IncList<X> {
	private Map<Integer, X> map;
	private Integer index;
	
	public IncListImpl() {
		this.map = new HashMap<>();
		this.index =  0;
	}
	@Override
	public int addNew(X x) {
		if(x != null) {
			map.put(this.index, x);
			//return this.index++;
			this.index++;
			return this.index-1; //perchè al primo giro(x es) ho inserito nell'indice 0 e ho incrementato index
		} else {
			throw new NullPointerException();
		}
	}

	@Override
	public X getElement(int index) {
		if(this.map.get(index) != null) {
			return this.map.get(index);
		} else {
			return null;
		}
	}

	@Override
	public int getPosition(X x){
		for (int i: this.map.keySet()){
			if (this.getElement(i).equals(x)){
				return i;
			}
		}
		return -1;
	}

	@Override
	public void remove(int index) {
		if(getElement(index) == null) {
			throw new IllegalArgumentException();
		} else {
			this.map.remove(index);
		}
	}

	@Override
	public int size() {
		return this.map.size();
	}

	@Override
	public Iterator<Integer> allOccurrences(X x) {
		Set<Integer> set = new HashSet<>();
		for(Integer i : this.map.keySet()) {
			if(this.map.get(i).equals(x)) {
				set.add(i);
			}
		}
		return set.iterator();
	}
}
