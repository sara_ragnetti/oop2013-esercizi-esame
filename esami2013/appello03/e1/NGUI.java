package esami2013.appello03.e1;

import javax.swing.*;

import java.awt.event.*;

@SuppressWarnings("serial")
public class NGUI extends JFrame {
	private JButton[] buttonRow;
	private Listener listener = new Listener();
	
	public NGUI(int n){
		this.buttonRow = new JButton[n];
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		
		for (int i=0; i<n; i++){
			this.buttonRow[i] = new JButton(""+i);
			this.buttonRow[i].addActionListener(listener);
			this.buttonRow[i].setActionCommand(""+i);
			panel.add(this.buttonRow[i]);
		}
		
		this.getContentPane().add(panel);
		this.pack();
		this.setVisible(true);
	}
	
	private class Listener implements ActionListener{
		private int last = -1; 

		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals("0")){
				last = 0;
			} else if (e.getActionCommand().equals(""+(last+1))){
				last++;
			} else {
				last = -1;
			}
			
			System.out.println(last);
			if (last == buttonRow.length-1){
				setVisible(false);
			}
		}
		
	}
}
