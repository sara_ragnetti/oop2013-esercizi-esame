package esami2013.appello01bis.e2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import esami2013.appello01bis.sol2.MyJList;

public class GUI extends JFrame {
	final private MyJList<String> jlist = new MyJList<>();
	final private JFrame frame = new JFrame();
	
	    public GUI() {
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    	frame.getContentPane().setLayout(new BorderLayout());
	    	
			frame.getContentPane().add(jlist, BorderLayout.CENTER);	
			JPanel panel = new JPanel(new FlowLayout());
			frame.getContentPane().add(panel, BorderLayout.SOUTH);	
			
			JButton add = new JButton("Add");
			
			// usa un costruttore che setta la larghezza del text field, espressa in columns
			JTextField text = new JTextField(10);
			JButton quit = new JButton("Print & Quit");
			
	        panel.add(add);	        
	        panel.add(text);
	        panel.add(quit);
	      
	        add.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					jlist.addElement(text.getText());
				}
	        });
	        
	        quit.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					for(String s : jlist.getAll()) {
						System.out.println(s);
					}
				}
	        });
	        
	    	frame.pack();
	        frame.setVisible(true);
	    }
	}

   