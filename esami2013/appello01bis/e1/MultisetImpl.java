package esami2013.appello01bis.e1;

	import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MultisetImpl<X> implements Multiset<X> {
		private List<X> list = new ArrayList<>();
		
		public MultisetImpl() {
		}

		@Override
		public void add(X x) {
			this.list.add(x);
		}

		@Override
		public void remove(X x) {
			if(!this.list.contains(x)) {
				throw new IllegalArgumentException();
			} else {
				this.list.remove(x);						
			}
		}

		@Override
		public int countElements(X x) {
			return (int) this.list.stream().
					filter(s->s.equals(x))
					.count();
		}

		@Override
		public int size() {
			return this.list.size();
		}

		@Override
		public Set<X> set() {
			Set<X> set = new HashSet<>();
			for(X x : list) {
				set.add(x);
			}
			return set;
		}
		
		//versione di Nicola
		@Override
		public boolean contains(Multiset<X> m) {
			
			int counter = ((MultisetImpl)m).getList().size();
			MultisetImpl<X> copy = new MultisetImpl<X>();
			this.list.forEach(e -> copy.add(e));
			
			for (Object e: ((MultisetImpl)m).getList()) {
				if (copy.getList().contains(e)) {
					counter--;
					copy.getList().remove(e);
				}
			}
			
			return counter == 0;
		}
		
		private List<X> getList() {
			return this.list;
		}

	}
