package esami2013.appello01.e1;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class RelationImpl<X, Y> implements Relation<X, Y> {
	Map<X, Set<Y>> map = new HashMap<>();
	
	private void checkXY(X x, Y y) {
		if(x == null || y == null) {
			throw new NullPointerException();
		}
	}
	
	@Override
	public void set(X x, Y y) {
		checkXY(x, y);
		
		//la chiave non esiste: la inserisco e creo un nuovo set di Y contenente y
		if(!this.map.containsKey(x)) {
			this.map.put(x, new HashSet<>());
			this.map.get(x).add(y);
		} else {
			// la chiave esiste quindi posso aggiungere il valore (essendo un hashSet, se c'è già non viene inserito)
			this.map.get(x).add(y);
		}
	}

	@Override
	public void unset(X x, Y y) {
		checkXY(x, y);
		if(this.map.containsKey(x)) {
			if(this.map.get(x).contains(y)) {
				if(this.map.get(x).size() <= 1) {
					this.map.remove(x, y);
				}
			}
			this.map.get(x).remove(y);
		}	
	}

	@Override
	public boolean holds(X x, Y y) {
		checkXY(x, y);
		return this.map.containsKey(x) && this.map.get(x).contains(y);	
	}

	@Override
	public int size() {
		return this.map.values().stream()
				.flatMap(Set::stream)
				.collect(Collectors.toList())
				.size();
	}

	@Override
	public Set<Y> relatedTo(X x) {
		if(x != null) {
			return this.map.containsKey(x) ? this.map.get(x) : new HashSet<>();
			}
			else {
				throw new NullPointerException();
		} 	
	}

}
