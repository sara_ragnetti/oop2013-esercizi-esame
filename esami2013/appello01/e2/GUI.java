package esami2013.appello01.e2;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class GUI extends PrimeListImpl {
	private PrimeListImpl primeList;
	
    private final JFrame frame = new JFrame();
	protected boolean stop = false;

    public GUI() {
    	this.primeList = new PrimeListImpl();
    	
        final JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        panel.setAlignmentX(FlowLayout.TRAILING);
        
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        final JButton next = new JButton("Next");
        panel.add(next);
        
        final JLabel label = new JLabel("Prime: ");
        panel.add(label);
        
        next.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(final ActionEvent e) {
				label.setText("Prime: "+ primeList.next());
            }
        });

        final JButton show = new JButton("Show & Read");
        panel.add(show);
        
        show.addActionListener(new ActionListener() {
        	int position = 0;
        	@Override
            public void actionPerformed(final ActionEvent e) { 
            	for(Integer i : primeList) {
            		position++;
            		System.out.println("Numero primo in posizione" + position + ": " + i);
            	}
            }
        });
      
    	frame.pack();
        frame.setVisible(true);
    }
}
