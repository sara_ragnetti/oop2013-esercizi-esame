package esami2013.appello02.e1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import javax.swing.JTextField;

@SuppressWarnings("serial")
public class FiboGUI extends JFrame {
	private final JFrame frame = new JFrame();
	private static String FILE_NAME = null;
	private Fibonacci numero;
	
	public FiboGUI(String nomeFile) {
		this.FILE_NAME = System.getProperty("user.home")+ File.separator + nomeFile;
		this.numero = new Fibonacci(1);
		
		final JPanel panel = new JPanel();
		frame.setContentPane(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		
		final JTextField text = new JTextField(10);
		final JButton start = new JButton("Start");
		final JButton next = new JButton("Next");
		panel.add(text);
		panel.add(start);
		panel.add(next);
		next.setEnabled(false);
		
		start.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try(
					final OutputStream file = new FileOutputStream(FILE_NAME);
					final OutputStream bstream = new BufferedOutputStream(file);
					final DataOutputStream dstream = new DataOutputStream(bstream);
					){
					List<String> list = new ArrayList<>();
					list.addAll(Arrays.asList(text.getText()));		
					for(String i: list){
						dstream.writeLong(Long.parseLong(i));
					};
					dstream.writeLong(-1);		
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
					
				} catch (IOException e1) {
					e1.printStackTrace();
				};

				start.setEnabled(false);
				next.setEnabled(true);
			}		
		});
		/* 

 * A questo punto, ad ogni pressione di Next si recuperi dal file il prossimo numero della serie di Fibonacci e 
 * lo si mostri nel testo del pulsante Next (metodo setText), ad esempio avendo "Next (13)" per indicare che 
 * il numero letto è il 13.
 * Quando non vi sono più numeri da leggere (lo si scopre trovando il -1) si disattivi il pulsante Next e si chiuda 
 * il file
 */
		next.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try(
					final InputStream file2 = new FileInputStream(FILE_NAME);
					final InputStream bstream = new BufferedInputStream(file2);
					final DataInputStream dstream2 = new DataInputStream(file2);
					){	
						
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
						
					} catch (IOException e1) {
						e1.printStackTrace();
					}
			}
		});
		
		frame.pack();
		frame.setVisible(true);
		
	}
}
