package esami2013.appello02.e2;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class GraphImpl<N, W> implements Graph<N, W> {
	// strutture per rappresentare il mio grafo:
	// un set per i nodi N
	private Set<N> nodes = new HashSet<>();
	
	//una mappa per gli archi con chiave = coppia di nodi e valore = peso associato
	private Map<Pair<N,N>,W> edges = new HashMap<>(); 
	
	public GraphImpl(){}
	
	private boolean nCheck(N n) {
		if(hasNode(n)) {
			throw new IllegalArgumentException();
		} return false;
	}
	
	@Override
	public void addNode(N n) {
		if(!nCheck(n)) {
			this.nodes.add(n);
		}	
	}

	@Override
	public void removeNode(N n) {
		for(Pair<N,N> pair : edges.keySet()) {
			if(pair.getX().equals(n) || pair.getY().equals(n)) {
				throw new IllegalArgumentException();
			}
		} this.nodes.remove(n);
	}

	@Override
	public boolean hasNode(N n) {
		return this.nodes.contains(n);
	}

	/*
	 * Adds an edge between an ordered pair of nodes, with given non-null weight.
	 * An edge between those two nodes should not be already there, otherwise IllegalArgumentException is thrown 
	 */
	@Override
	public void addEdge(N n1, N n2, W w) {
		if(w != null) {
			for(W w1 : edges.values()) {
				if(w1.equals(w)) {
					edges.entrySet().stream()
					.filter(s->(s.getKey().getX().equals(n1) && s.getKey().getY().equals(n2)));
				}
			}
		}
		
	}

	/*
	 * Removes the edge between an ordered pair of nodes.
	 * Nothing happens if the edge is not there 
	 */
	@Override
	public void removeEdge(N n1, N n2) {
		// TODO Auto-generated method stub
		
	}

	/*
	 * Return the weight of the edge between the ordered pair of nodes.
	 * Returns null if one such edge does not exist 
	 */
	@Override
	public W weight(N n1, N n2) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * Returns the set of all nodes of the graph
	 */
	@Override
	public Set<N> allNodes() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * Returns the set of all edges starting from node n: for each, it gives target node and weight
	 */
	@Override
	public Set<Pair<N, W>> outgoing(N n) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * Returns the set of neighbours of n (namely, the same nodes returned by method outgoing). 
	 * Note that n does not belong to this set, in general.
	 */
	@Override
	public Set<N> neighbours(N n) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * Returns the set of neighbours of all nodes in s.
	 */
	@Override
	public Set<N> neighbours(Set<N> s) {
		// TODO Auto-generated method stub
		return null;
	}

}
