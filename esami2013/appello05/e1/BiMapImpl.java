package esami2013.appello05.e1;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class BiMapImpl<X, Y> implements BiMap<X, Y> {
	private Map<X, Y> mapx;
	private Map<Y, X> mapy;

	public BiMapImpl() {
		this.mapx = new HashMap<>();
		this.mapy = new HashMap<>();
	}
	
	/*
	 * adds an association between x and y
	 * x and y should not be null and not be already associated to other elements 
	 */
	@Override
	public void put(X x, Y y) {
		if(x == null || y == null) {
			throw new NullPointerException();
		} else if(getX(y) != null || getY(x) != null) {  //se non è null: valori già in relazione
			throw new IllegalArgumentException();
		} else { 										 // quei valori sono null allora posso aggiungere una nuova relazione
			this.mapx.put(x, y);
			this.mapy.put(y, x);
		}
	}

	@Override
	public X getX(Y y) {
		return this.mapy.get(y);
	}

	@Override
	public Y getY(X x) {
		return this.mapx.get(x);
	}
	
	@Override
	public boolean hasXY(X x, Y y) {
		if(this.mapx.get(x) != null && this.mapx.get(x).equals(y)) {
			return true;
		} return false;
	}

	@Override
	public void remove(X x, Y y) {
		if (!hasXY(x,y)){
			throw new IllegalArgumentException();
		} 
		this.mapx.remove(x);
		this.mapy.remove(y);
	}

	@Override
	public int size() {
		return this.mapy.size();
	}

	@Override
	public Set<X> allX() {
		return this.mapx.keySet();
	}

	@Override
	public Set<Y> allY() {
		return this.mapy.keySet();
	}

}
