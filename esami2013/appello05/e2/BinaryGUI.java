package esami2013.appello05.e2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/*
 * I pulsanti zero e uno servono per digitare una sequenza binaria (dal bit meno significativo al più significativo)
 * che dovrà comparire sulla sinistra mano a mano che viene digitata.
 * Il pulsante reset la deve azzerare (ossia deve rendere la sequenza di bit vuota), in modo che la si possa 
 * riscrivere daccapo.
 * Il pulsante compute calcola il valore decimale della sequenza binaria e lo riporta sulla destra, come indicato
 * in figura (infatti, il 13 corrisponde al numero binario 1101).
*/
public class BinaryGUI extends JFrame {
	private static final long serialVersionUID = -2649228250369560392L;
	private final Model model = new ModelImpl();
	
	private final JButton uno = new JButton("1");
	private final JButton zero = new JButton("0");
	private final JButton reset = new JButton("reset");
	private final JButton compute = new JButton("compute");
	private final JPanel panel = new JPanel();
	private final JLabel bitSequence = new JLabel();
	private final JLabel result = new JLabel();
	
	public BinaryGUI() {
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setContentPane(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		
		panel.add(bitSequence);
		panel.add(uno);
		panel.add(zero);
		panel.add(reset);
		panel.add(compute);
		panel.add(result);
		
		uno.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				model.one();
				bitSequence.setText(model.getString());				
			}
		});
		
		zero.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				model.zero();
				bitSequence.setText(model.getString());				
			}	
		});
		
		reset.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				model.reset();
				bitSequence.setText(model.getString());
			}	
		});
		
		compute.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				result.setText("" + model.compute());
			}	
		});
		
		this.pack();
		this.setVisible(true);
	}
}
