package esami2013.appello05.e2;

import java.util.ArrayList;
import java.util.List;

public class ModelImpl implements Model {
	private List<Boolean> sequence = new ArrayList<>();
	
	public ModelImpl() {}
	
	@Override
	public void one() {
		sequence.add(true);
	}

	@Override
	public void zero() {
		sequence.add(false);	
	}

	@Override
	public void reset() {
		sequence.clear();
	}

	@Override
	public int compute() {
		int val = 0;
		int i =  0;
		
		for(Boolean b : sequence) {
			if(b==true) {
				val = val + 1*(2^i);
				i++;
			} i++;
		} return val;
	}

	@Override
	public String getString() {
		String s = "";
		for(Boolean b : sequence) {
			if(b) {
				s += "1";
			} else {
				s += "0";
			}
		} return s;
	}

}
