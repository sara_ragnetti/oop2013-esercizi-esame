package esami2013.appello05.e2;

public interface Model {
	void one();
	
	void zero();
	
	void reset();
	
	int compute();
	
	String getString();
}
