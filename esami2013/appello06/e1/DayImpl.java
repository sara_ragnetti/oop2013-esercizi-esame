package esami2013.appello06.e1;

public class DayImpl implements Day {
	private final int dayNum;
	private final Month month;
	private final int year;
	
	public DayImpl(int dayNum, Month month, int year) {
		if(dayNum > 31 || dayNum <= 0 || (month.compareTo(Month.FEBRUARY) == 0 && dayNum > 29)) {
			throw new IllegalArgumentException();
		}
		
		this.dayNum = dayNum;
		this.month = month;
		this.year = year;
	}
	
	@Override
	public int getDay() {
		return this.dayNum;
	}

	@Override
	public Month getMonth() {
		return this.month;
	}

	@Override
	public int getYear() {
		return this.year;
	}

	@Override
	public Day next() {
		return null;
	}

	@Override
	public int compareTo(Day d) {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
